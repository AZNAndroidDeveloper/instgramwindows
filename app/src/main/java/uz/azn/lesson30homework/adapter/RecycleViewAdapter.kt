package uz.azn.lesson30homework.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uz.azn.lesson30homework.R
import uz.azn.lesson30homework.model.InfomationModel
import uz.azn.lesson30homework.onItemSelected.OnItemSelectedClick

class RecycleViewAdapter(var allInfo: MutableList<InfomationModel>) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {
    lateinit var onItemSelectedClick: OnItemSelectedClick
    var ishHave = true
    var isBookMark = true

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView = itemView.findViewById<ImageView>(R.id.persom_image)
        val name = itemView.findViewById<TextView>(R.id.name_tv)
        val hear = itemView.findViewById<ImageView>(R.id.image_heart)
        val book_mark = itemView.findViewById<ImageView>(R.id.image_book)
        fun bind(info: InfomationModel) {
            imageView.setImageResource(info.image)
            name.text = info.name

        }

        init {
            imageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemSelectedClick.onItemSelect(adapterPosition)
                }
            }
            hear.setOnClickListener {
                if (ishHave) {
                    hear.setImageResource(R.drawable.ic_red_heart)
                    ishHave = false
                } else {
                    hear.setImageResource(R.drawable.ic_heart)
                    ishHave = true
                }
            }
            book_mark.setOnClickListener {
                if (isBookMark) {
                    book_mark.setImageResource(R.drawable.ic_bookmark_dark)
                    isBookMark = false
                } else {
                    book_mark.setImageResource(R.drawable.ic_bookmark)
                    isBookMark = true
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.image_item, null, false)
        return MyViewHolder(inflater)
    }

    override fun getItemCount(): Int = allInfo.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(allInfo[position])


    }

    fun setOnItemClickedListener(listener: OnItemSelectedClick) {
        this.onItemSelectedClick = listener
    }

}