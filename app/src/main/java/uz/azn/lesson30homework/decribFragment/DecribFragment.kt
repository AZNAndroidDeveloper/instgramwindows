package uz.azn.lesson30homework.decribFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import uz.azn.lesson30homework.R
import uz.azn.lesson30homework.model.InfomationModel

class DecribFragment(mcontext:Context?) : Fragment() {

private lateinit var list:MutableList<InfomationModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view  =  inflater.inflate(R.layout.fragment_decrib, container, false)

        val postion = arguments!!.getInt("postion")
        val allInfo = allInfo()
        val id = allInfo[postion].image
        val name = allInfo[postion].name
        val image = view.findViewById<ImageView>(R.id.image_person)
        val personName = view.findViewById<TextView>(R.id.name_tv)
        image.setImageResource(id)
        personName.text = name

        return view
    }

    fun allInfo():MutableList<InfomationModel>{
        list= arrayListOf()
        list.add(InfomationModel(R.drawable.ralando,"Ranaldo","sport.uz"))
        list.add(InfomationModel(R.drawable.messi,"Messi","sport.com"))
        list.add(InfomationModel(R.drawable.inesta,"Inesta","sport.ru"))
        list.add(InfomationModel(R.drawable.aguero,"Aguero","sport.sd"))
        list.add(InfomationModel(R.drawable.mbappe,"Mbappe","sport.kg"))
        list.add(InfomationModel(R.drawable.odil,"Odil Axmedov","sport.kz"))
        list.add(InfomationModel(R.drawable.zidane,"Zidane","sport.com"))
        list.add(InfomationModel(R.drawable.pogba,"Pogba","sport.uz"))
        return list
    }


}