package uz.azn.lesson30homework.introFragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.azn.lesson30homework.R
import uz.azn.lesson30homework.adapter.RecycleViewAdapter
import uz.azn.lesson30homework.decribFragment.DecribFragment
import uz.azn.lesson30homework.model.InfomationModel
import uz.azn.lesson30homework.onItemSelected.OnItemSelectedClick


class IntroFragment(var mContext:Context?) : Fragment() {

    init {
        mContext = context
    }
    lateinit var list:MutableList<InfomationModel>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val view =  inflater.inflate(R.layout.fragment_intro, container, false)
        val recycle = view.findViewById<RecyclerView>(R.id.recycle_view)

        recycle.layoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        val recycleViewAdapter = RecycleViewAdapter(allInfo())
        recycle.adapter = recycleViewAdapter

        recycleViewAdapter.setOnItemClickedListener(object :OnItemSelectedClick{
            override fun onItemSelect(postion: Int) {
                val bundle = Bundle()
                bundle.putInt("postion",postion)
                Log.d("TAG", "onItemSelect: $postion")
                val decribFragment = DecribFragment(mContext)
                decribFragment.arguments = bundle
                val manager = fragmentManager
                manager?.beginTransaction()?.replace(R.id.frame_layout_intro,decribFragment)?.addToBackStack(decribFragment.toString())?.commit()
            }
        })
        return view
    }

    fun allInfo():MutableList<InfomationModel>{
         list= arrayListOf()
        list.add(InfomationModel(R.drawable.ralando,"Ranaldo","sport.uz"))
        list.add(InfomationModel(R.drawable.messi,"Messi","sport.com"))
        list.add(InfomationModel(R.drawable.inesta,"Inesta","sport.ru"))
        list.add(InfomationModel(R.drawable.aguero,"Aguero","sport.sd"))
        list.add(InfomationModel(R.drawable.mbappe,"Mbappe","sport.kg"))
        list.add(InfomationModel(R.drawable.odil,"Odil Axmedov","sport.kz"))
        list.add(InfomationModel(R.drawable.zidane,"Zidane","sport.com"))
        list.add(InfomationModel(R.drawable.pogba,"Pogba","sport.uz"))
return list
    }
}