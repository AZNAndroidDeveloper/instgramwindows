package uz.azn.lesson30homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.lesson30homework.databinding.ActivityMainBinding
import uz.azn.lesson30homework.introFragment.IntroFragment

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val manager  = supportFragmentManager
        manager.beginTransaction().replace(binding.frameLayout.id,IntroFragment(this)).commit()
    }
}